package com.newelderstudio.nukimarvelstravel;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import java.text.NumberFormat;
import java.util.Locale;

public class Summary extends AppCompatActivity {
    TextView tujuan,tanggalBerangkat,tanggalPulang,harga,jumlahTiket,txPulang;
    NumberFormat numberFormat;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_summary);
        tujuan = findViewById(R.id.sumTujuan);
        tanggalBerangkat = findViewById(R.id.sumTanggalBerangkat);
        tanggalPulang = findViewById(R.id.sumPulang);
        jumlahTiket = findViewById(R.id.sumJumlahTiket);
        harga = findViewById(R.id.sumHarga);
        txPulang = findViewById(R.id.txSumPulang);
        if (!getIntent().getBooleanExtra("pulangPergi",false)){
            tanggalPulang.setVisibility(View.GONE);
            txPulang.setVisibility(View.GONE);
        }else{
            tanggalPulang.setText(getIntent().getStringExtra("tanggalPulang")+" " + getIntent().getStringExtra("waktuPulang")+" WIB");
        }
        tujuan.setText(getIntent().getStringExtra("tujuan"));
        tanggalBerangkat.setText(getIntent().getStringExtra("tanggalBerangkat")+" " + getIntent().getStringExtra("waktuBerangkat")+" WIB");
        jumlahTiket.setText(getIntent().getStringExtra("jumlahTiket"));
        numberFormat = NumberFormat.getNumberInstance(new Locale("in","ID"));

        harga.setText("Rp " + numberFormat.format(getIntent().getIntExtra("total",0)));

    }

    public void confirm(View view){
        Home.currentSaldo -= getIntent().getIntExtra("total",0);
        finish();

    }
}
